use std::{env, thread, time};
use std::net::{TcpListener, SocketAddr, TcpStream};
use std::io::{Error, Read, BufReader, BufRead, Write};
use std::time::{SystemTime, SystemTimeError, Duration};

fn main() {
    // Goblin arg parsing, -l for listen, arg after -p MUST be address
    let args: Vec<String> = env::args().collect();
    let mut client = true;
    let mut target= "127.0.0.1:9999";

    let mut nextisport = false;
    let starttime = SystemTime::now();

    for arg in &args {
        if nextisport {
            println!("{:?}", arg);
            target = arg;
            nextisport = false;
        }

        if arg == "-l" {
            client = false;
        }

        if arg == "-p" {
            nextisport = true;
        }
    }

    if client {
        // Client
        let mut stream = TcpStream::connect(target);
        match stream {
            Ok(mut stream) => {
                println!("{:?}", stream);
                let mut result = Ok(());

                while result.is_ok() {
                    let now = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
                        Ok(v) => { v.as_micros() }
                        Err(e) => {println!("Clock fucked, {:?}", e); return;}
                    };
                    let t = format!("{}\n", now);
                    let t= t.as_bytes();

                    result = stream.write_all(&t);
                    thread::sleep(time::Duration::from_millis(100));
                }

                println!("Write error, {:?}", result);
            }
            Err(e) => {
                println!("Error connecting, {:?}", e);
                return;
            }
        }

    }else{
        // Server
        let listener = match TcpListener::bind(target){
            Ok(v) => {v}
            Err(e) => {println!("Error listening, {:?}", e); return;}
        };

        // accept connections and process them serially
        let stream = listener.accept();
        match stream{
            Ok((mut stream, addr)) => {
                println!("Connection accepted");
                println!("{:?}", stream);
                println!("{:?}", addr);

                let mut reader = BufReader::new(stream).lines();

                while let line = reader.next() {
                    match line {
                        None => { println!("None"); return; }
                        Some(v) => {
                            match v {
                                Ok(line) => {
                                    println!("{}", line);
                                    let rtime: u128 = line.parse().unwrap();
                                    let now = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
                                        Ok(v) => { v.as_micros() }
                                        Err(e) => {println!("Clock fucked, {:?}", e); return;}
                                    };

                                    println!("Time difference: {}ms; time elapsed total: {}s", (now-rtime)/1000, starttime.elapsed().unwrap().as_secs());

                                }
                                Err(e) => {
                                    println!("Read error, {:?}", e);
                                }
                            }
                        }
                    }
                }
            }
            Err(e) => {
                println!("Error accepting connection, {:?}", e);
                return;
            }
        }
    }
}
